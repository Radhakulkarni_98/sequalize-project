const express = require("express");
const app = express();
const port = 8080;
require("./config/database")
const userCntrl = require("./controller/userController");
 const bookCntrl = require("./controller/bookingController")
 const userBookCntrl=require("./controller/userbookController")
 const couponCntrl = require("./controller/couponController")
 const orderCntrl = require("./controller/orderController")



app.get("/",(req,res)=>{
    res.send("api called......")
})

     
app.get("/crud",userCntrl.crudAPI)                               //crud opration        
app.get("/user",userCntrl.allUser);                              //called controller 
app.get("/query",userCntrl.queryData)                            //query data
// app.get("/validate",userCntrl.validation)                        //validations




//booking routes
app.get("/userBooking",bookCntrl.userBookingOne)        //insert records API
app.get("/findData",bookCntrl.findData)

//user_booking routes one to one
//GET request
app.get("/insertmore",bookCntrl.insertFn)               //Insert record api
app.get("/findAllUser",userCntrl.findAllUser)
app.get("/findByNameEmail", userBookCntrl.findByNameEmail)
app.get("/findOnePost",userBookCntrl.findOnePost)
app.get("/findUserBooking",userBookCntrl.findUserBooking)
app.get('/findUserById',userBookCntrl.findUserById)
// app.get("/:uid", userBookCntrl.findByUrl);


//user_order routes one to one---Get reqest
app.get("/orderall",orderCntrl.orderAll)
app.get ("/findAllOrders",orderCntrl.findAllOrders)
app.get("/oneOrder",orderCntrl.oneOrder)
app.get("/allUsers",orderCntrl.allUser)
app.get("/userOrders",orderCntrl.userOrders)
app.get("/orderAndUser",orderCntrl.orderAndUser)

//user_coupon routes one to one
app.get("/insertCoupon",couponCntrl.insertCoupon)
app.get("/allCoupons",couponCntrl.allCoupons)
app.get("/userAndCoupon",couponCntrl.userAndCoupon)


//POST request
//  app.post("/createNewUser",userCntrl.createNewUser)

//one to many
// app.get('/updatepost',userBookCntrl.updatePost)

app.listen(port,()=>{
    console.log(`server is running on ${port}`)
})