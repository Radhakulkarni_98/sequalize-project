const db = require("../config/database");
const User = db.user;
const Book = db.book;


const findUserById=async(req,res)=>{

    const data =await User.findAll({
         where:{user_id:7},
         include:Book
    })
    res.status(200).json(data)
}


const findByNameEmail = async (req,res)=>{
    const data =await User.findAll({
        attributes:['firstname','lastname','email'],
        include:[{
            model:Book,
            attributes:['hotelname']
        }
        ]
     
    })
    res.status(200).json(data)
}



//which hotel has which user 
const findOnePost = async(req,res)=>{
    const data = await Book.findAll({
        where:{b_id:"6"},
        attributes:['hotelname'],
        include:User
    })
    res.status(200).json(data)
}

//find all bookings and thier users 
const findUserBooking =async(req,res)=>{
    const data = await Book.findAll({
        include:User
    })
    res.status(200).json(data)
}


// const findByUrl = async(req, res) => {
//     const uid = req.params.uid;
//     await User.findByPk(uid)
//       .then(data => {
//         if (data) {
//           res.send(data);
//         } else {
//           res.status(404).send({
//             message: `Cannot find User with id=${uid}.`
//           });
//         }
//       })
//       .catch(err => {
//         res.status(500).send({
//           message: "Error retrieving User with id=" + uid
//         });
//       });
//   };





// const updatePost = async(req,res)=>{
//     const data = await Book.update({
//    'hotelname':'saj'},
//    {where:[{
//     b_id:"7"
//    }]
      
//    })
//    res.status(200).json({"message":"post updated successfully",data})
// }




module.exports={
    findByNameEmail,
    findOnePost,
    // updatePost
    findUserBooking,
    findUserById,
    // findByUrl
    
  
}