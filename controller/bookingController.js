const { user } = require("../config/database");
const db=require("../config/database");
const Book = db.book 


const userBookingOne=async (req,res)=>{
    var data = await Booking.bulkCreate([
        {b_id:1,hotelname:"rosegold",user_id:6},
        {b_id:2,hotelname:"miami",user_id:7},
        {b_id:3,hotelname:"tata",user_id:8},
        {b_id:4,hotelname:"saj",user_id:9},

]);
    res.status(200).json(data)   
}

const insertFn =async(req,res)=>{
    const data = await Book.bulkCreate([
        {b_id:5,hotelname:"marriot",user_id:10},
        {b_id:6,hotelname:"ramigrand",user_id:11},
        {b_id:7,hotelname:"fivestar",user_id:12},
        {b_id:8,hotelname:"holidays",user_id:13},
        {b_id:9,hotelname:"ruby",user_id:14},
    ])
    res.status(200).json(data)
}


const findData=async(req,res)=>{
    const data = await Book.findAll({})
    res.status(200).json(data);
}


module.exports={
    userBookingOne,
    findData,
    insertFn
}