const { user } = require("../config/database");
const db = require("../config/database");
const Coupon=db.coupon;



const insertCoupon =async(req,res)=>{
const data =await Coupon.bulkCreate([
    {c_id:1,title:"coupon11",user_id:"6",o_id:"1"},
    {c_id:2,title:"coupon12",user_id:"7",o_id:"2"},
    {c_id:3,title:"coupon13",user_id:"8",o_id:"3"},
    {c_id:4,title:"coupon14",user_id:"9",o_id:"4"},
    {c_id:5,title:"coupon15",user_id:"10",o_id:"5"},
    {c_id:6,title:"coupon16",user_id:"11",o_id:"6"}
   
])
res.status(200).json(data)
}


const allCoupons =async(req,res)=>{
    const data = await Coupon.findAll({})
    res.status(200).json(data)
}

const userAndCoupon = async (req,res)=>{
    const data = await Coupon.findAll({
        attributes:[
            "title"
        ],
        include:[{
            model:user,
            attributes:["firstname","lastname"]
        }]
    })
}

module.exports={
    insertCoupon,
    allCoupons,
    userAndCoupon
};

