const db = require("../config/database");
const Order = db.order
const User = db.user

const orderAll=async(req,res)=>{
    const data = await Order.bulkCreate([
        {o_id:1,order_title:"burger",user_id:"6"},
        {o_id:2,order_title:"maggie",user_id:"7"},
        {o_id:3,order_title:"shahi panner",user_id:"8"},
        {o_id:4,order_title:"fried rice",user_id:"9"},
        {o_id:5,order_title:"dosa",user_id:"10"},
        {o_id:6,order_title:"panipuri",user_id:"11"},
        {o_id:7,order_title:"chat",user_id:"12"},
        {o_id:8,order_title:"fried rice",user_id:"13"},
     
     
    ])
    res.status(200).json(data)
}

const findAllOrders =async(req,res)=>{
    const data = await Order.findAll({})
    res.status(200).json(data)
}

//Which user has which order 
const oneOrder=async(req,res)=>{
    const data = await Order.findAll({
        include:User
    })
  res.status(200).json(data)
}


const allUser = async(req,res)=>{
    const data = await User.findAll({})
    res.status(200).json(data)
}

const userOrders = async(req,res)=>{
    const data = await User.findAll({
    include:Order,
      attributes:[
          "firstname",
          "lastname",
          "email",
     
      ],
    

    })
    res.status(200).json(data)
}

const orderAndUser=async(req,res)=>{

    const data = await Order.findAll({
        attributes:[
            "order_title"
        ],
        include:[{
            model:User,
            attributes:["firstname","lastname"]
        }]
    
    })
    res.status(200).json(data)
}

module.exports={
    findAllOrders,
    orderAll,
    oneOrder,
    allUser,
    userOrders,
    orderAndUser
}