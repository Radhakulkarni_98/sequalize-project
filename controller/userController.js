// const { Sequelize,Op } = require("../config/database");
const { includes } = require('lodash');
const{Sequelize,Op, Model}=require('sequelize');
const { book } = require('../config/database');
const db = require("../config/database");
const User = db.user;
const Book = db.book

//user table controller
var allUser=async(req,res)=>{

    var data = await User.bulkCreate([
  
        {user_id:5,firstname:"riya",lastname:"patil",email:"riya@gmail.com",age:21,city:"mumbai"},
        {user_id:6,firstname:"shloka",lastname:"ahuja",email:"shloka21@gmail.com",age:20,city:"jaipur"},
        {user_id:7,firstname:"bela",lastname:"joshi",email:"bela@gmail.com",age:19,city:"goa"},
        {user_id:8,firstname:"simi",lastname:"singh",email:"singh@gmail.com",age:18,city:"pune"}
     
    
    ] );
    res.status(200).json(data);
}

//crud opration controller 
var crudAPI=async(req,res)=>{

//insert query 

// var resp = await User.bulkCreate([
//     { user_id:11,firstname:"ayushi",lastname:"ahuja",email:"ayu@gmail.com",age:22,city:"bhopal"},
//     { user_id:12,firstname:"ruchi",lastname:"singh",email:"rooh@gmail.com",age:18,city:"bhopal"},
//     { user_id:13,firstname:"vaibhavi",lastname:"kijbile",email:"vaibhavi21@gmail.com",age:23,city:"jaipur"},
//     { user_id:14,firstname:"ketaki",lastname:"simpi",email:"kat@gmail.com",age:21,city:"gao"},
    
    
// ]);
// res.status(200).json(resp);
// }

// update
//   var resp = await User.update( { firstname:"radha"},
//  {where:{
//      user_id:5
//  }}
// );

//delete
//   var resp = await User.destroy( 
//  {
//      where:
//     {
//      user_id:5
//  }}
// );
// res.status(200).json(resp);
//   }

//truncate
//   var resp = await User.destroy( 
//  {
//     truncate:true
//  }
//   )
//   res.status(200).json(resp)

}
 const findAllUser =async(req,res)=>{
     const data=await User.findAll({})
     res.status(200).json(data)
 }

//  const createNewUser =async(req,res)=>{
   
//         try {
       
//               const {firstname, lastname, email, age,city } = req.body;
//               const newUser = await User.create({
//                 user_id: newId,
//                 firstname,
//                 lastname,
//                 email,
//                 age,
//                 city
//               });
//               res.status(200).json({
//                 msg: `New User Added Successfully`,
//               });
            
          
//         } catch (error) {
//           res.status(400).json({
//             error: error.message,
//           });
//         }
//     }


 
 

 



//query data
var queryData=async(req,res)=>{
//**************select*********************
// var resp = await User.findAll({
//     attributes:[
//         'firstname',
//         'lastname',
//         [Sequelize.fn('Count',Sequelize.col('firstname')),'nameCount']  //it will display how many times first name is getting displayed(count function)

//     ]
// });

//Include & exclude
//**************exclude****************
// var resp = await User.findAll({
//     attributes:{
//         exclude:['city']
//     }
// });
// res.status(200).json(resp);
// }

//***********include****************
// var resp = await User.findAll({
//     attributes:{
//         include:['city']
//     }
// });
// res.status(200).json(resp);


//**********condition*********/
// var resp = await User.findAll({
// where:{
// //   user_id:{
// //     [Op.eq]:5                            //WHERE CONDITION USES OPERATORS WHICH WE TAKE AS OP FROM SEQUALIZE PACKAGE(operators:eq,like etc)
// //   },
//  email:{
//      [Op.like]:"%@gmail.com"
//  }
// }
// });
// res.status(200).json(resp);
}


// const validation=async(req,res)=>{
//     const data =await User.create({user_id:10,firstname:"saloni",lastname:"singh",email:"saluu@gmail.com",age:20,city:"jaipur"})
//     res.status(200).json(data)
// }




module.exports={
    allUser,
    crudAPI,
    queryData,
    findAllUser,
    //  createNewUser
    // validation,

}



//.create is not function error === because i have not exported db module and not 
//retutn user in modules