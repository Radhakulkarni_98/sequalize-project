const {Sequelize,DataTypes} = require("sequelize");
require("../modal/userModal")
require("../modal/bookingModal");
require("../modal/couponModal")
require("../modal/orderModal");

const sequelize = new Sequelize("bookingsdb","root","",{
    host:"localhost",
    dialect:"mysql"
})

sequelize.authenticate()
.then(()=>{
    console.log("database is connected....")
}).catch(err=>{
    console.log(err)
})

const db={}
db.Sequelize=Sequelize;
db.sequelize=sequelize;



//user modal
db.user=require("../modal/userModal")(sequelize,DataTypes);

//booking modal
db.book = require("../modal/bookingModal")(sequelize,DataTypes);  

//coupon modal
db.coupon = require("../modal/couponModal")(sequelize,DataTypes)   

//order modal
db.order = require("../modal/orderModal")(sequelize,DataTypes)

db.sequelize.sync({force:false})
.then(()=>{
    console.log("table created....")
}).catch(err=>{
    console.log("error encounted..",err)
})


//Associations
db.user.hasOne(db.book ,{foreignKey:'user_id'});           //defualt userID 
db.book.belongsTo(db.user,{foreignKey:'user_id'})          //ONE TO ONE //when we use one to one relation we need to use belongs to as well



db.user.hasOne(db.order,{foreignKey:'user_id'});
db.order.belongsTo(db.user,{foreignKey:'user_id'});


// db.user.hasOne(db.coupon,{foreignKey:'user_id',foreignKey:'o_id'});
// db.coupon.belongsTo(db.user,{foreignKey:'user_id',foreignKey:'o_id'});


module.exports=db;