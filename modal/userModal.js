// module.exports=(sequelize,DataTypes)=>{
//     const user=sequelize.define("users",{
//         user_id:{
//             type:DataTypes.INTEGER,
//             allowNull:false,
//             primaryKey:true,
//             autoIncrement:true
//         },
//         firstname:{
//             type:DataTypes.STRING,
//             allowNull:false,
           
//         },
//         lastname:{
//             type:DataTypes.STRING,
//             allowNull:false,
//         },
//         email:{
//             type:DataTypes.STRING,
//             allowNull:false,
//         },
//         age:{
//             type:DataTypes.INTEGER,
//         },
//         city:{
//             type:DataTypes.STRING,
//         }

//     })
//     return user
// }


module.exports = (sequelize, DataTypes) =>
{
    const User = sequelize.define('users',{
        user_id : {
            type : DataTypes.STRING,
            allowNull: false,
            unique: true,
        },
        firstname : 
        {
            type : DataTypes.STRING,
            allowNull: false,
        },
        lastname : 
        {
            type : DataTypes.STRING,
            allowNull: false,
        },
        email : 
        {
            type : DataTypes.STRING,
            allowNull: false,
            validate : {
                isEmail : true
            },
            unique: true,
        },
        city : 
        {
            type : DataTypes.STRING,
            allowNull: false,
        },
        isAgent : 
        {
            type : DataTypes.ENUM(
                "true", 
                'false',
                ),
             defaultValue: 'false', 
        }
    },
    {
        timestamps: true, 
        createdAt: true, 
        updatedAt: true, 
    })

    return User
}