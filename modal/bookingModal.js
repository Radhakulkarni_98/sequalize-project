module.exports=(sequelize,DataTypes)=>{
    const booking = sequelize.define("bookings",{
        b_id:{
            type:DataTypes.INTEGER,
            allowNull:false,
            primaryKey:true,
            autoIncrement:true
        },
        hotelname:{
            type:DataTypes.STRING,
            allowNull:false,
        },
        user_id:{
            type:DataTypes.INTEGER
        }
    })
    return booking;
}