module.exports =(sequalize,DataTypes)=>{
    const coupon= sequalize.define("coupons",{
        c_id:{
            type:DataTypes.INTEGER,
            allowNull:false,
            primaryKey:true
        },
        title:{
            type:DataTypes.STRING
        },
        user_id:{
            type:DataTypes.INTEGER
        },
        o_id:{
            type:DataTypes.INTEGER
        }
    })
    return coupon
}