module.exports=(sequelize,DataTypes)=>{
    const Order = sequelize.define("orders",{
        o_id:{
            type:DataTypes.INTEGER,
            allowNull:false,
            primaryKey:true
        },
        order_title:{
            type:DataTypes.STRING
        },
        user_id:{
            type:DataTypes.INTEGER
        }
       
    })
    return Order
}